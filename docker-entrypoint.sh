#!/usr/bin/env bash

[[ $DEBUG ]] && set -x




function parse_mongo_url {
  local MONGO_URL="$1"

  local AUTH
  local HOST_PORT
  local PROTO
  local URL

  PROTO="$(echo ${MONGO_URL} | grep :// | sed -e's,^\(.*://\).*,\1,g')"
  URL="${MONGO_URL/${PROTO}/}"

  AUTH="$(echo ${URL} | grep @ | rev | cut -d@ -f2- | rev)"
  if [ -n ${AUTH} ]; then
    URL="$(echo ${URL} | rev | cut -d@ -f1 | rev)"
  fi

  MONGO_PASSWORD="$(echo ${AUTH} | grep : | cut -d: -f2-)"
  if [ -n "${MONGO_PASSWORD}" ]; then
    MONGO_USER="$(echo ${AUTH} | grep : | cut -d: -f1)"
  else
    MONGO_USER=${AUTH}
  fi

  HOST_PORT="$(echo ${URL} | cut -d/ -f1)"
  MONGO_PORT="$(echo ${HOST_PORT} | grep : | cut -d: -f2)"
  if [ -n "${MONGO_PORT}" ]; then
    MONGO_HOST="$(echo ${HOST_PORT} | grep : | cut -d: -f1)"
  else
    MONGO_PORT=27017
    MONGO_HOST="${HOST_PORT}"
  fi

  [[ $DEBUG ]] && echo "URL=$URL"

  MONGO_DATABASE="$(echo ${URL} | grep / | cut -d/ -f2- | cut -d'?' -f1)"
}

if [[ -z $DB_URL ]]; then
  echo "Missing DB_URL variable, cannot connect to mongodb"
  exit 1
fi

parse_mongo_url $DB_URL

[[ $DEBUG ]] && echo "MONGO: $MONGO_HOST:$MONGO_PORT auth $MONGO_USER:$MONGO_PASSWORD db=$MONGO_DATABASE"

/usr/bin/wait-for-it.sh ${MONGO_HOST}:${MONGO_PORT}

if [[ -n $SSL_CA_BUNDLE_URL ]]; then
  wget --quiet -O- $SSL_CA_BUNDLE_URL > /tmp/ca-bundle.pem
  if [[ $? -gt 0 ]]; then
    echo "Error downloading CA Bundle from ${SSL_CA_BUNDLE_URL}, cannot continue."
    exit 1
  fi
  echo "Successfully downloaded CA Bundle from ${SSL_CA_BUNDLE_URL} and writing to /tmp/ca-bundle.pem"
fi

exec "$@"
