FROM node:22-alpine

WORKDIR /srv

COPY package*.json /srv/

RUN npm install --only production

COPY . .

EXPOSE 8000

ENV NODE_ENV=production

HEALTHCHECK --start-period=5s --interval=10s --timeout=5s CMD wget --spider http://localhost:8000/health || exit 1   

ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /usr/bin/
RUN apk add --no-cache bash && \
    chmod +x /usr/bin/wait-for-it.sh

COPY docker-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT [ "/docker-entrypoint.sh" ]
CMD [ "npm", "start" ]
