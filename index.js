const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');
require('dotenv').config();
const swaggerUi = require('swagger-ui-express');
const openApiDocumentation = require('./openapi');
const fs = require('fs');


const cors = require('cors');

const app = express();

app.use(bodyParser.json({limit: '10mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));

app.use(morgan('tiny'));

// DB CONNECTION
const url = process.env.DB_URL;

// Do we use SSL connection?
const ssl_ca_bundle_url = process.env.SSL_CA_BUNDLE_URL
let options;
if (typeof ssl_ca_bundle_url != "undefined" && ssl_ca_bundle_url.length > 0)
{
  const caContent = [fs.readFileSync("/tmp/ca-bundle.pem")];
  options = {
    sslValidate: true,
    sslCA: caContent,
    useNewUrlParser: true,
    useUnifiedTopology: false
  };
} else {
  options = {
    useNewUrlParser: true,
    useUnifiedTopology: false
  };
}
const connect = mongoose.connect(url, options);


connect.then(function () {
  console.log('Connected correctly to database');
}, function (err) {
  console.log(err);
});

// use it before all route definitions
app.use(cors());

// APIs
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openApiDocumentation));

const healthRouter = require('./routes/healthcheck');
app.use('/health', healthRouter);

const formRouter = require('./routes/form');
app.use('/', formRouter);

app.set('port', (process.env.PORT || 8000));

app.listen(app.get('port'), function () {
  console.log('Node app is running on port', app.get('port'));
});

module.exports = app; // for testing
