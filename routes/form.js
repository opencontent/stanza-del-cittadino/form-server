const express = require('express');
const bodyParser = require('body-parser');
const response = require('../modules/json_response');
const utils = require('../modules/utils');
const models = require('../models/form');
const i18n = require('../modules/i18n');

const router = express.Router();

router.use(bodyParser.json());

// Get form by id
router.route('/form/:id')
  // getForm: retrieve a form
  .get(function (req, res) {
    models.Form.findById(req.params.id).then(function (form) {
      if (form) {
        response(res, 'OK', form, 200);
      } else {
        response(res, 'Form Not Found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, 'Invalid Id', err, 400);
    });
  })
  // Deletes a form by Id
  .delete(function (req, res) {
    let id = req.params.id;

    models.Form.findOneAndDelete({_id: id}).then(function (form) {
      if (form) {
        response(res, 'OK', form, 200);
      } else {
        response(res, 'Form Not Found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, `Invalid id ${id}`, err, 400);
    });
  });

router.route('/form/:id/schema')
  // getSchema: compute a form Schema
  .get(function (req, res) {
    models.Form.findById(req.params.id).then(function (form) {
      if (form) {
        utils.extractSchema(form.components, function (schema) {
          response(res, 'OK', schema, 200);
        });
      } else {
        response(res, 'Form not found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, 'Invalid Id', err, 400);
    });
  });

router.route('/form/:id/i18n-labels')
  // getSchema: compute a form Schema
  .get(function (req, res) {
    models.Form.findById(req.params.id).then(function (form) {
      if (form) {
        utils.getTranslationsFromForm(form.components, function (translations) {
          response(res, 'OK', Object.keys(translations).sort(), 200);
        });
      } else {
        response(res, 'Form not found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, 'Invalid Id', err, 400);
    });
  });

router.route('/form/:id/i18n')
  // getSchema: compute a form Schema
  .get(function (req, res) {
    let lang = req.query.lang;

    let query = {'form': req.params.id}
    if (lang) {
      query['lang'] = lang
    }

    models.Translation.find(query).then(function (translations) {
      let data = {};

      if (translations.length !== 0) {
        translations.forEach(function (translation) {
          data[translation.lang] = {...translation.data, ...i18n[translation.lang ? translation.lang : 'it']};
        })
      }

      // Add default static translations if no translations has been found
      lang = lang ? lang : 'it';
      if (!data.hasOwnProperty(lang)) {
        // Use locale static translations if exist else use default language static translations
        data[lang] = i18n.hasOwnProperty(lang) ? i18n[lang] : i18n['it'];
      }

      response(res, 'OK', utils.restoreTranslationKeys(data), 200);
    }).catch(function (err) {
      console.error(err)
      response(res, 'Invalid form Id', err, 400);
    });
  })
  .post(function (req, res) {
    let payload = req.body
    if (typeof payload !== "object") {
      return response(res, "The payload sent is not an object", req.body, 400);
    }

    let languages = Object.keys(req.body);
    if (languages.length < 1) {
      return response(res, "At least one translation is required", req.body, 400);
    }

    let translations = {};
    let translation_data = utils.clearTranslationKeys(req.body);

    Object.keys(req.body).forEach(function (lang) {
      Object.keys(req.body[lang]).forEach(function (key) {
        let new_key = key.replace(new RegExp(/\./g, 'g'), '[dot]').replace(new RegExp(/\$/g, 'g'), '[dollar]');
        translation_data[new_key] = req.body[lang][key]
      })
      let data = {
        "form": req.params.id,
        "lang": lang,
        "data": translation_data[lang],
      }
      models.Translation.create(data).then(function (new_translation) {
        translations[lang] = new_translation.data;
        if (Object.keys(translations).length === Object.keys(req.body).length) {
          let response_data = utils.restoreTranslationKeys(translations);
          response(res, 'Created', response_data, 201);
        }
      }).catch(function (err) {
        console.error(err)
        response(res, err, req.body, 400);
      });
    })
  })
  .put(function (req, res) {
    let payload = req.body
    if (typeof payload !== "object") {
      return response(res, "The payload sent is not an object", req.body, 400);
    }

    let languages = Object.keys(req.body);
    if (languages.length < 1) {
      return response(res, "At least one translation is required", req.body, 400);
    }

    let data = utils.clearTranslationKeys(req.body);
    let translations = {};
    Object.keys(req.body).forEach(function (lang) {
      models.Translation.findOne({lang: lang, form: req.params.id}).then(function (translation) {
        if (translation) {
          translation.data = data[lang];
          translation.modified = Date.now();
          translation.save();

          translations[lang] = translation.data;

          if (Object.keys(translations).length === Object.keys(req.body).length) {
            response(res, 'OK', utils.restoreTranslationKeys(translations), 200);
          }
        } else {
          return response(res, 'The requested form does not exists', req.originalUrl,
            404);
        }
      }).catch(function (err) {
        console.error(err)
        return response(res, 'Invalid parameter', err, 400);
      });
    })
  })
  .patch(function (req, res) {
    let payload = req.body
    if (typeof payload !== "object") {
      response(res, "The payload sent is not an object", req.body, 400);
    }

    let languages = Object.keys(req.body);
    if (languages.length < 1) {
      return response(res, "At least one translation is required", req.body, 400);
    }

    let data = utils.clearTranslationKeys(req.body);
    let translations = {};
    Object.keys(req.body).forEach(function (lang) {
      models.Translation.findOne({lang: lang, form: req.params.id}).then(function (translation) {
        if (translation) {
          translation.data = {...translation.data, ...data[lang]};
          translation.modified = Date.now();
          translation.save();

          translations[lang] = translation.data;

          if (Object.keys(translations).length === Object.keys(req.body).length) {
            response(res, 'OK', utils.restoreTranslationKeys(translations), 200);
          }
        } else {
          return response(res, 'The requested form does not exists', req.originalUrl,
            404);
        }
      }).catch(function (err) {
        console.error(err)
        return response(res, 'Invalid parameter', err, 400);
      });
    })
  });

router.route('/printable/form/:id')
  // getSchema: compute a form Schema
  .get(function (req, res) {
    models.Form.findById(req.params.id).then(function (form) {
      if (form) {
        response(res, 'OK', wizardConverter(form), 200);
      } else {
        response(res, 'Form not found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, 'Invalid Id', err, 400);
    });
  });

router.route('/printable/:id')
  // getSchema: compute a form Schema
  .get(function (req, res) {
    models.Form.findById(req.params.id).then(function (form) {
      if (form) {
        response(res, 'OK', wizardConverter(form), 200);
      } else {
        response(res, 'Form not found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, 'Invalid Id', err, 400);
    });
  });

router.route(/^\/[a-zA-Z0-9_-]+\/schema$/)
  // Get schema
  .get(function (req, res) {
    const path = req.originalUrl.split('/', 2).pop();
    models.Form.findOne({path: path}).then(function (form) {
      if (form) {
        utils.extractSchema(form.components, function (schema) {
          response(res, 'OK', schema, 200);
        });
      } else {
        response(res, 'Form not found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, 'Invalid path', err, 400);
    });
  });

router.route('/form/')
  // listConsents: list forms registered
  .get(function (req, res) {
    // Default: show only components
    var options = {
      tags: {$nin: ['custom', 'basic']},
      deprecated: { $ne: true }
    };

    var selected = {};
    if (req.query.name) {
      options.name = {
        '$regex': new RegExp('.*' + req.query.name + '.*', 'i'),
      };
    }
    if (req.query.show_deprecated && req.query.show_deprecated.toLowerCase() === 'true' && options.hasOwnProperty('deprecated')) {
      delete options.deprecated
    }
    if (req.query.select) {
      req.query.select.split(',').forEach(function (select) {
        selected[select] = true;
      });
    }
    if (req.query.exclude_tags) {
      let excluded = [];
      req.query.exclude_tags.split(',').forEach(function (tag) {
        excluded.push(tag);
      });
      options.tags.$nin = excluded;
    }

    models.Form.find(options).select(selected).then(function (results) {
      response(res, 'OK', results, 200);
    }).catch(function (err) {
      console.error(err)
      response(res, 'Bad request', err, 400);
    });
  })
  // createConsent: create a form
  .post(function (req, res) {
    models.Form.create(req.body).then(function (form) {
      response(res, 'Created', form, 201);
    }).catch(function (err) {
      console.error(err)
      response(res, err, req.body, 400);
    });
  });

router.route('/:path')
  // Get form by path
  .get(function (req, res) {
    models.Form.findOne({path: req.params.path}).then(function (result) {
      if (result) {
        response(res, 'OK', result, 200);
      } else {
        response(res, 'Form not Found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, 'Invalid path', err, 400);
    });
  })
  // Edit Form
  .put(function (req, res) {
    models.Form.findById(req.body._id).then(function (form) {
      if (form) {
        form.title = req.body.title;
        form.name = req.body.name;
        form.path = req.body.path;
        form.description = req.body.description;
        form.display = req.body.display;
        form.tags = req.body.tags;
        form.components = req.body.components;
        form.modified = Date.now();
        form.deprecated = req.body.deprecated || false;
        form.save();
        response(res, 'OK', form, 200);
      } else {
        response(res, 'The requested form does not exists', req.originalUrl,
          404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, 'Invalid parameter', err, 400);
    });
  })
  // delete Form
  .delete(function (req, res) {
    models.Form.findOneAndDelete({path: req.params.path}).then(function (form) {
      if (form) {
        response(res, 'OK', form, 200);
      } else {
        response(res, 'Invalid parameter path', req.originalUrl, 400);
      }
    });
  });

router.route(/^\/[a-zA-Z0-9_-]+\/submission$/)
  // Get submissions
  .get(function (req, res) {
    const path = req.originalUrl.split('/', 2).pop();
    models.Form.findOne({path: path}).select('_id').then(function (form) {
      if (form) {
        models.Submission.find({form: form._id}).then(function (results) {
          response(res, 'OK', results, 200);
        }).catch(function (err) {
          console.error(err)
          response(res, 'Invalid form', err, 400);
        });
      } else {
        response(res, 'Form not found', req.originalUrl, 404);
      }
    });
  }).post(function (req, res) {
  // Extract path
  const path = req.originalUrl.split('/', 2).pop();
  models.Form.findOne({path: path}).select('_id').then(function (form) {
    if (form) {
      delete req.body['submit'];
      models.Submission.create({
        data: req.body,
        form: form._id,
        status: null,
      }).then(function (submission) {
        response(res, 'OK', submission, 201);
      }).catch(function (err) {
        console.error(err)
        response(res, 'Invalid data', err, 400);
      });
    } else {
      response(res, 'Form not found', req.originalUrl, 404);
    }
  });
});

router.route(/^\/[a-zA-Z0-9_-]+\/submission\/.[^\/]+\/?$/)
  // get submission by id
  .get(function (req, res) {
    const id = req.originalUrl.split('/', 4).pop();
    models.Submission.findById(id).then(function (submission) {
      if (submission) {
        response(res, 'OK', submission, 200);
      } else {
        response(res, `Submission with id ${id} not found`, req.originalUrl,
          404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, `Invalid id ${id}`, err, 400);
    });
  })
  // Edit submission
  .put(function (req, res) {
    const id = req.originalUrl.split('/', 4).pop();
    models.Submission.findById(id).then(function (submission) {
      if (submission) {
        submission.data = req.body;
        submission.modified = Date.now();
        submission.save();
        response(res, 'OK', submission, 200);
      } else {
        response(res, `Submission with id ${id} not found`, req.originalUrl,
          404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, `Invalid id ${id}`, err, 400);
    });
  })
  // Delete Submission
  .delete(function (req, res) {
    const id = req.originalUrl.split('/', 4).pop();
    models.Submission.findOneAndDelete({_id: id}).then(function (form) {
      if (form) {
        response(res, 'OK', form, 200);
      } else {
        response(res, 'Submission Not Found', req.originalUrl, 404);
      }
    }).catch(function (err) {
      console.error(err)
      response(res, `Invalid id ${id}`, err, 400);
    });
  });

module.exports = router;

function wizardConverter(form) {
  if (form.display === 'wizard') {
    // Metodo 1: conversione semplice: espansione delle pagine in verticale
    form.display = 'form';
  }
  return disable(form);
}

function disable(form, callback) {
  let disabledForm = JSON.parse(JSON.stringify(form))

  if (disabledForm.components) {
    // Disable form components
    disabledForm.components.forEach(function (component, index) {
      disabledForm.components[index] = disable(component);
    })
  } else if (disabledForm.columns) {
    // Disable columns components
    disabledForm.columns.forEach(function (column, index) {
      disabledForm.columns[index] = disable(column);
    })
  } else {
    // Simple component
    // Disable JS custom calculated values
    if (disabledForm.type === 'select' && ['custom', 'url'].includes(disabledForm['dataSrc'])) {
      disabledForm.dataSrc = 'custom';
      if (disabledForm.data && disabledForm.data.custom) {
        // Disable custom js
        disabledForm.data.custom = '';
      }
    }
    // Disable JS custom default values
    if (disabledForm.customDefaultValue) {
      disabledForm.customDefaultValue = '';
    }
    // Disable JS custom calculated values
    if (disabledForm.calculateValue) {
      disabledForm.calculateValue = '';
    }
    // Disable JS validation
    if (disabledForm.validate) {
      disabledForm.validate.custom = '';
    }
  }
  return disabledForm;
}
