var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var formSchema = new Schema({
  modified: {
    type: Date,
    default: Date.now,
  },
  title: {
    type: String,
    required: true,
  },
  display: {
    type: String,
    default: 'form',
  },
  type: {
    type: String,
    default: 'form',
  },
  name: {
    type: String,
    required: true,
    unique: true,
  },
  path: {
    type: String,
    required: true,
    unique: true,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  components: [],
  description: {
    type: String,
    required: true
  },
  tags: {
    type: [],
    default: ['custom']
  },
  deprecated: {
    type: Boolean,
    default: false
  }
});
formSchema.pre('save', function(next) {
  this.increment();
  return next();
});

const Forms = mongoose.model('Form', formSchema);

var submissionSchema = mongoose.Schema({
  data: {},
  form: {
    type: mongoose.Types.ObjectId,
  },
  modified: {
    type: Date,
    default: Date.now,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  status: {
    type: Boolean,
  }
});

submissionSchema.pre('save', function(next) {
  this.increment();
  return next();
});

const Submissions = mongoose.model('Submission', submissionSchema);

let translationSchema = mongoose.Schema({
  form: {
    type: mongoose.Types.ObjectId,
  },
  data: {},
  lang: {
    type: String,
    required: true,
  },
  modified: {
    type: Date,
    default: Date.now,
  },
  created: {
    type: Date,
    default: Date.now,
  },
})

translationSchema.index({
  lang: 1,
  form: 1
}, {
  unique: true
});


translationSchema.pre('save', function(next) {
  this.increment();
  return next();
});

const Translations = mongoose.model('Translation', translationSchema);

module.exports = {Form: Forms, Submission: Submissions, Translation:Translations};
