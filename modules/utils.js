const models = require('../models/form');
const waterfall = require('async-waterfall');


function getTranslationsFromForm(components, callback) {
  let translations = {}
  if (components.length === 0) {
    return callback(translations)
  }
  let numComponents = components.length;
  let completed = 0;

  waterfall([
    function (next) {
      components.forEach(function (component) {
          if (component.form) {
            // Nested Form
            models.Form.findById(component.form).then(function (nestedForm) {
              if (nestedForm) {
                getTranslationsFromForm(nestedForm.components, function (nestedTranslations) {
                  translations = {...nestedTranslations, ...translations}
                  completed += 1;
                  if (completed === numComponents) {
                    next(null, translations);
                  }
                });
              } else {
                // No form
                next("Misisng form", translations);
              }
            });
          } else if (!component.input || component.input === 'false') {
            // Layout
            // COLUMNS
            if (component.type === 'columns') {
              getTranslationsFromForm(component.columns, function (results) {
                translations = {...results, ...translations};
                completed += 1;
                if (completed === numComponents) {
                  next(null, translations);
                }
              });
              // PANEL
            } else if (component.type === "panel") {
              translations[component.title] = component.title;
              getTranslationsFromForm(component.components, function (results) {
                translations = {...results, ...translations};
                completed += 1;
                if (completed === numComponents) {
                  next(null, translations);
                }
              });
              // FIELDSET
            } else if (component.type === 'fieldset') {
              if (component.legend) {
                translations[component.legend] = component.legend;
              }
              if (component.tooltip) {
                translations[component.tooltip] = component.tooltip;
              }
              getTranslationsFromForm(component.components, function (results) {
                translations = {...results, ...translations};
                completed += 1;
                if (completed === numComponents) {
                  next(null, translations);
                }
              });
              // COLUMN, WELL
            } else if (['column', 'well'].includes(
              component.type)) {
              getTranslationsFromForm(component.components, function (results) {
                translations = {...results, ...translations};
                completed += 1;
                if (completed === numComponents) {
                  next(null, translations);
                }
              });
              // TABLE
            } else if (component.type === 'table') {
              let _ = 0;
              if (component.numRows === undefined || component.numRows === null) {
                component.numRows = component.rows.length;
              }
              if (component.numCols === undefined || component.numCols === null && component.rows.length > 0) {
                component.numCols = component.rows[0].length
              }
              component.rows.forEach(function (row) {
                row.forEach(function (cell) {
                  getTranslationsFromForm(cell.components, function (results) {
                    translations = {...results, ...translations};
                    _ += 1;
                    if (_ === (component.numRows * component.numCols)) {
                      completed += 1;
                      if (completed === numComponents) {
                        next(null, translations);
                      }
                    }
                  });
                });
              });
              // TABS
            } else if (component.type === 'tabs') {
              let numTabs = 0;
              component.components.forEach(function (tab) {
                getTranslationsFromForm(tab.components, function (results) {
                  translations = {...results, ...translations};
                  numTabs += 1;
                  if (numTabs === tab.components.length) {
                    completed += 1;
                    if (completed === numComponents) {
                      next(null, translations);

                    }
                  }
                });
              });
              // CONTENT, HTML
            } else if (['content', 'htmlelement'].includes(component.type)) {
              if (component.html) {
                translations[component.html] = component.html
              }
              if (component.content) {
                translations[component.content] = component.content
              }
              completed += 1;
              if (completed === numComponents) {
                next(null, translations);
              }
            } else if (component.components) {
              //input not defined
              getTranslationsFromForm(component.components, function (results) {
                translations = {...results, ...translations};
                completed += 1;
                if (completed === numComponents) {
                  next(null, translations);

                }

              });
            }
            // Input Type
          } else if (['datagrid', 'editgrid'].includes(component.type)) {
            getTranslationsFromForm(component.components, function (results) {
              translations = {...results, ...translations};
              completed += 1;
              if (completed === numComponents) {
                next(null, translations);
              }
            });
          } else {
            // Standard component: no subcomponents
            translations[component.label] = component.label;
            if (component.placeholder)
              translations[component.placeholder] = component.placeholder;
            if (component.description)
              translations[component.description] = component.description;
            if (component.tooltip)
              translations[component.tooltip] = component.tooltip;
            if (component.errorLabel)
              translations[component.errorLabel] = component.errorLabel;
            if (component.prefix)
              translations[component.prefix] = component.prefix;
            if (component.suffix)
              translations[component.suffix] = component.suffix;
            if (component.type === 'select' && component.data && component.data.values) {
              component.data.values.forEach((item) => {
                translations[item.label] = item.label
              })
            }
            if (component.defaultValue)
              translations[component.defaultValue] = component.defaultValue;
            if (!['checkbox', 'radio', 'selectboxes'].includes(component.type))
              if (component.defaultValue)
                translations[component.defaultValue] = component.defaultValue;
            if (component.validate && component.validate.customMessage) {
              translations[component.validate.customMessage] = component.validate.customMessage;
            }
            if (component.values) {
              component.values.forEach(function (value) {
                translations[value.label] = value.label
              })
            }
            completed += 1;
            if (completed === numComponents) {
              next(null, translations);
            }
          }
        },
      );
    },
    function (translations) {
      callback(translations);
    },
  ]);
}

/**
 * Extracts submission schema from a given array of components
 * @param components: array of components to extract schema from
 * @param callback: function called when schema is ready
 */
function extractSchema(components, callback) {
  if (components.length === 0) {
    return callback(null)
  }
  let numComponents = components.length;
  let completed = 0;
  waterfall([
    function (next) {
      let schema = {};
      components.forEach(function (component) {
          if (component.form) {
            // Nested Form
            models.Form.findById(component.form).then(function (nestedForm) {
              // Extract Schema from Nested Form
              if (!nestedForm) {
                completed += 1;
                if (completed === numComponents) {
                  next(null, null);
                }
              } else {
                extractSchema(nestedForm.components, function (nestedSchema) {
                  schema[component.key] = {data: nestedSchema};
                  completed += 1;
                  if (completed === numComponents) {
                    next(null, schema);
                  }
                });
              }
            });
          } else if (!component.input || component.input === 'false') {
            // Layout
            // COLUMNS
            if (component.type === 'columns') {
              extractSchema(component.columns, function (results) {
                schema = {...schema, ...results};
                completed += 1;
                if (completed === numComponents) {
                  next(null, schema);
                }
              });
              // COLUMN, FIELDSET, PANEL, WELL
            } else if (['column', 'fieldset', 'panel', 'well'].includes(
              component.type)) {
              extractSchema(component.components, function (results) {
                schema = {...schema, ...results};
                completed += 1;
                if (completed === numComponents) {
                  next(null, schema);
                }
              });
              // TABLE
            } else if (component.type === 'table') {
              let _ = 0;
              if (component.numRows === undefined || component.numRows === null) {
                component.numRows = component.rows.length;
              }
              if (component.numCols === undefined || component.numCols === null && component.rows.length > 0) {
                component.numCols = component.rows[0].length
              }
              component.rows.forEach(function (row) {
                row.forEach(function (cell) {
                  extractSchema(cell.components, function (results) {
                    schema = {...schema, ...results};
                    _ += 1;
                    if (_ === (component.numRows * component.numCols)) {
                      completed += 1;
                      if (completed === numComponents) {
                        next(null, schema);
                      }
                    }
                  });
                });
              });
              // TABS
            } else if (component.type === 'tabs') {
              let numTabs = 0;
              component.components.forEach(function (tab) {
                extractSchema(tab.components, function (results) {
                  schema = {...schema, ...results};
                  numTabs += 1;
                  if (numTabs === tab.components.length) {
                    completed += 1;
                    if (completed === numComponents) {
                      next(null, schema);

                    }
                  }
                });
              });
              // CONTENT, HTML
            } else if (['content', 'htmlelement'].includes(component.type)) {
              completed += 1;
              if (completed === numComponents) {
                next(null, schema);
              }
            } else if (component.components) {
              //input not defined
              extractSchema(component.components, function (results) {
                schema = {...schema, ...results};
                completed += 1;
                if (completed === numComponents) {
                  next(null, schema);

                }

              });
            }
            // Input Type
          } else if (['datagrid', 'editgrid'].includes(component.type)) {
            extractSchema(component.components, function (results) {
              schema[component.key] = [results];
              completed += 1;
              if (completed === numComponents) {
                next(null, schema);
              }
            });
          } else {
            // Standard component: no subcomponents
            if (component.type === 'selectboxes') {
              schema[component.key] = {};
              component.values.forEach(function (value) {
                schema[component.key][value.value] = value.label;
              });
              completed += 1;
              if (completed === numComponents) {
                next(null, schema);
              }
            } else if (component.type === 'button') {
              // skip buttons
              completed += 1;
              if (completed === numComponents) {
                next(null, schema);
              }
            } else {
              // Add type inside schema
              schema[component.key] = {
                label: component.label,
                type: component.type
              };
              // Old version, no type
              // schema[component.key] = component.label;
              completed += 1;
              if (completed === numComponents) {
                next(null, schema);
              }
            }
          }
        },
      );
    },
    function (schema) {
      callback(schema);
    },
  ]);
}

function clearTranslationKeys(data) {
  let cleared_data = {};
  Object.keys(data).forEach(function (lang) {
    let cleared = {};
    Object.keys(data[lang]).forEach(function (key) {
      let new_key = key
        .replace(new RegExp(/\./g, 'g'), '[dot]')
        .replace(new RegExp(/\$/g, 'g'), '[dollar]');
      cleared[new_key] = data[lang][key];
    })
    cleared_data[lang] = cleared;
  });
  return cleared_data;
}

function restoreTranslationKeys(data) {
  let restored_data = {};
  Object.keys(data).forEach(function (lang) {
    let restored = {};
    Object.keys(data[lang]).forEach(function (key) {
      let new_key = key
        .replace(new RegExp(/\[dollar]/g, 'g'), '$')
        .replace(new RegExp(/\[dot]/g, 'g'), '.');
      restored[new_key] = data[lang][key];
    })
    restored_data[lang] = restored;
  });
  return restored_data;
}

module.exports = {
  extractSchema: extractSchema,
  getTranslationsFromForm: getTranslationsFromForm,
  clearTranslationKeys: clearTranslationKeys,
  restoreTranslationKeys: restoreTranslationKeys
};
